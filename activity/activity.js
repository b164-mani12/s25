//s25 Activity

//#2 --- 
db.fruits.aggregate([
            { $unwind : "$origin" },
            { $group : { _id : "$origin" , fruits : { $sum : 1 } } }
        ]);


//#3 --- 
db.fruits.aggregate([
            { $match : {onSale: true} },
            { $count: "onSale"}
]);

//#4 ---
db.fruits.aggregate([
    { $match: { stock: { $gte: 20 } } },
    { $count: "enoughStock" }
])

//#5
db.fruits.aggregate([
        { $match : {onSale: true} },
        { $group : { _id : "$supplier_id" , fruits : { $sum : 1 } } },
        { price: { $avg: "$price" }}
]); 

//#6
db.fruits.aggregate([
        { $match : {onSale: true} },
        { $group : { _id : "$supplier_id" , average_price: { $avg: "$price"}}},
]);

//#7 
db.fruits.aggregate([
        { $match : {onSale: true} },
        { $group : { _id : "$supplier_id" , max_price: { $max: "$price"}}},
]);

//#8
db.fruits.aggregate([
        { $match : {onSale: true} },
        { $group : { _id : "$supplier_id" , min_price: { $min: "$price"}}},
]);
